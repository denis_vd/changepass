//
//  ChangePassView.swift
//  Roomer
//
//  Created by Denis Voskoboynik on 12/5/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit

class ChangePassView: UIView {
  @objc var callback: RCTBubblingEventBlock?
  
  @IBOutlet weak var oldPassField: UITextField!
  @IBOutlet weak var newPassField: UITextField!
  @IBOutlet weak var errorLabel: UILabel!
  @IBOutlet weak var button: UIButton!
  
  @objc var oldPassword: NSString = ""
  
  override func awakeFromNib() {
    super.awakeFromNib()
    configureUI()
  }
  
  @IBAction func tapAction(_ sender: Any) {
    guard let old = oldPassField.text, !old.isEmpty else {
      errorLabel.text = "Enter Old password"
      return
    }
    
    guard let new = newPassField.text, !new.isEmpty  else {
      errorLabel.text = "Enter New password"
      return
    }
    
    if old == String(oldPassword) {
      let dict = NSDictionary(object: NSString(string: new), forKey: NSString("key"))
      let result = dict as! [AnyHashable : Any]
      callback?(result)
      errorLabel.text = ""
    } else {
      errorLabel.text = "Wrong password"
    }
  }
  
  func configureUI() {
    oldPassField.placeholder = "Enter old password"
    newPassField.placeholder = "Enter new password"
    button.setTitle("Change password", for: .normal)
    oldPassField.delegate = self
  }
}

extension ChangePassView: UITextFieldDelegate {
  func textFieldDidBeginEditing(_ textField: UITextField) {
    errorLabel.text = ""
  }
}
