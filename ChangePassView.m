//
//  ChangePassView.m
//  Roomer
//
//  Created by Denis Voskoboynik on 12/5/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "React/RCTViewManager.h"

@interface RCT_EXTERN_MODULE(ChangePass, RCTViewManager)
RCT_EXPORT_VIEW_PROPERTY(oldPassword, NSString)
RCT_EXPORT_VIEW_PROPERTY(callback, RCTBubblingEventBlock)
@end
