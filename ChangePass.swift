//
//  ChangePass.swift
//  Roomer
//
//  Created by Denis Voskoboynik on 12/5/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
  
  class func makeFromXibWith(fileOwner: Any?) -> UIView? {
    let nibs = Bundle.main.loadNibNamed(String(describing: self), owner: fileOwner, options: nil)
    
    return (nibs as? [UIView])?.last
  }
}

@objc(ChangePass)
class ChangePass: RCTViewManager {
  
  override func view() -> UIView! {
    let view =  ChangePassView.makeFromXibWith(fileOwner: nil) as? ChangePassView
    return view
  }
  
  @objc override static func requiresMainQueueSetup() -> Bool {return true}
}
